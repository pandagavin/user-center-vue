---
home: true
heroImage: '/img/avatar.jpg'
heroText: 用户手册
# tagline: 🚀构建HPC生态环境
actionText: 快速开始 →
actionLink: "/manual/system/"
bannerBg: none # auto => 网格纹背景(有bodyBgImg时无背景)，默认 | none => 无 | '大图地址' | background: 自定义背景样式       提示：如发现文本颜色不适应你的背景时可以到palette.styl修改$bannerTextColor变量

features: # 可选的
  - title: 性能强劲
    details: 使用 Intel Xeon Processor (Skylake) CPU x86_64 ，单节点内存 128GB，统一共享存储，提供强劲计算能力
  - title: 使用便捷
    details: 支持 Enrivonment Modules 环境变量管理工具，软件环境一键切换；支持 Slurm 资源调度，管理作业非常方便
  - title: 专业服务
    details: 为用户个性化定制应用环境，提供专业、快捷的技术服务，让您使用超算无后顾之忧

# 文章列表显示方式: detailed 默认，显示详细版文章列表（包括作者、分类、标签、摘要、分页等）| simple => 显示简约版文章列表（仅标题和日期）| none 不显示文章列表
postList: none
---

## ⚡️关于

大家好！这里是中芯超算的用户中心首页，我们提供集群资源的用户使用手册。在您使用过程中遇到任何问题，欢迎与我们联系！

## 🔔交流

- **WeChat**: 

<center><img src="/img/wechat.jpg" width="300" align=center /></center>

- **Email**:  <a href="mailto:pandagavin@foxmail.com">pandagavin@foxmail.com</a>

- **联系电话**: 18612281061

## 许可证

[MIT](https://github.com/xugaoyi/vuepress-theme-vdoing/blob/master/LICENSE)

Copyright (c) 2021-present PandaGavin

<br/>

